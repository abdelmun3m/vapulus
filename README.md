## to run docker-compose 
1. cd project dir 
2. please note that ports 3000 and 8080 should be avilable 
3. run docker-compose build
4. run docker-compose up
5. open http://localhost:8080



## for backEnd 
1. node version 14.15.4
2. cd backend
3. npm i
4. configure .env file to set required varables
5. npm start


## for backEnd 
1. node version 14.15.4
2. cd frontend
3. npm i
4. configure .env file to set required varables
5. npm run serve