import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "about" */ '../views/Login.view.vue')
  },
  {
    path: '/create/:token',
    name: 'creatPassword',
    component: () => import(/* webpackChunkName: "about" */ '../views/SetPassword.view.vue')
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to, from, next) => {
  if (['Login', 'creatPassword'].indexOf(to.name) !== -1) {
    next()
  } else {

    if (store.state.user) {
      next()
    } else {
      return next('/login')
    }
  }
})

export default router
