/* eslint-disable @typescript-eslint/no-explicit-any */

import axios from 'axios';
import Vue from 'vue';
import store from '../store';

const http = axios.create({
    baseURL: process.env.VUE_APP_BACKEND_URL,
});

http.interceptors.request.use((conf) => {
    const config = { ...conf };
    const token = store.state.user ? store.state.user.token : null;
    if (token) {
        config.headers.Authorization = `Bearer ${token}`;
    };
    return config;
},
    (error) => error);

http.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

http.interceptors.response.use((response) => {
    return response.data;
}, (error) => {
    return Promise.reject(error.response);
});

export default class provider {
    
    static async userRegist(email, name) {
        const result = await http.post('/api/users/register', { email, name });
        return result;
    }

    static async createUser(email, name, password) {
        const result = await http.post('/api/users/createUser', { email, name, password });
        return result;
    }


    static async test(email, name) {
        const result = await http.post('/api/users/testToken', { email, name });
        return result;
    }


    static async verifyToken(token) {
        const result = await http.get(`/api/users/${token}`);
        return result;
    }

    static async creatAccount(token, password) {
        const result = await http.post(`/api/users/${token}`, { password });
        return result;
    }


    static async userLogin(email, password) {
        const result = await http.post(`/api/users/login`, { email, password });
        return result;
    }

    static getDocumentUrl() {
        return process.env.VUE_APP_BACKEND_URL + '/api/api-docs/'
    }
};