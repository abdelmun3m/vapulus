const MailService = require('../utils/email.service');
const userModel = require('./users.model');
var jwt = require('jsonwebtoken');
const { Logger, LOG_STRATEGYS } = require('../utils/log.service')
const LoggerContext = new Logger(LOG_STRATEGYS.CONSOLE, "USER_CONTROLLER");
class UserController {
    /**
     * 
     * @param {string} email 
     * @param {string} name 
     */
    static async userRegister(email, name) {
        const user = await userModel.getByEmail(email);
        if (user) {
            throw new Error('Sorrey! this email used by another user')
        }
        const mailService = new MailService();
        const emailText = UserController.getPasswordCreationMessage(email, name);
        const res = mailService.send(email, 'register', emailText, (error, info) => {
            if (error) {
                LoggerContext.log(email, 'faid recived an Email');
            } else {
                LoggerContext.log(email, ' recived an Email');
            }
        });
        LoggerContext.log(email, 'registerd')
        return email;
    }


    static async userAdd(email, name, password) {
        const user = await userModel.getByEmail(email);
        if (user) {
            throw new Error('Sorrey! this email used by another user')
        }
        await UserController.registerUser(email, name, password)
        LoggerContext.log(email, 'registerd')
        return user;
    }

    static getPasswordCreationMessage(email, name) {
        const token = UserController.createRegistrationToken(email, name);
        const url = `${process.env.frontURL}/create/${token}`
        const html = `
            click the link  to create account <a href='${url}'>${url}</a>`;
        return html
    }
    static createRegistrationToken(email, name) {
        const payload = {
            email,
            name
        };
        var token = jwt.sign(payload, process.env.jwtSecret,
            { expiresIn: process.env.tokenExpireIn });
        return token;
    }

    static createUserToken(user) {
        const payload = user;
        var token = jwt.sign(payload, process.env.jwtSecret);
        return token;
    }

    static verifyRegisterToken(token) {
        const payload = jwt.verify(token, process.env.jwtSecret);
        return payload;
    }

    static verifyUserToken(token) {
        const payload = jwt.verify(token, process.env.jwtSecret, {
            expiresIn: process.env.tokenExpireIn
        });
        return payload;
    }
    /**
     * 
     * @param {*} email 
     * @param {*} name 
     * @param {*} password 
     */
    static registerUser(email, name, password) {
        return userModel.add(email, name, password);
    }


    /**
     * 
     * @param {*} email 
     * @param {*} password 
     */
    static async userLogin(email, password) {
        console.log(email, password)
        const user = await userModel.getByEmail(email);
        if (!user || user.password !== password) {
            throw new Error('no user');
        }
        return user;
    }
}
module.exports = UserController;