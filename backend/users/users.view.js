const userController = require('./users.controller');
class UsersView {

    /**
     * 
     * @param {*} body.email
     * @param {*} body.name

     */
    static async postRegister(req, res, next) {
        try {
            const { email, name } = req.body;
            const userRegister = await userController.userRegister(email, name);
            res.send("check email" + userRegister);
        } catch (err) {
            return next(err)
        }

    }

    static async postCreateUser(req, res, next) {
        try {
            const { email, name, password } = req.body;
            const user = await userController.userAdd(email, name, password);
            res.send("User Created");
        } catch (err) {
            return next(err)
        }
    }

    /**
        * 
        * @param {*} params.token
        */
    static async verifyToken(req, res, next) {
        try {
            const { token } = req.params;
            const usedData = await userController.verifyRegisterToken(token);
            res.send({ ...usedData, token })
        } catch (err) {
            return next(err);
        }

    }


    /**
       * 
       * @param {*} params.token
       *  @param {*} body.password
       */
    static async putUserPassword(req, res, next) {
        try {
            const { token } = req.params;
            const { password } = req.body
            const { email, name } = await userController.verifyRegisterToken(token);
            const usedData = await userController.registerUser(email, name, password)
            res.send(usedData)
        } catch (err) {
            return next(err);

        }

    }


    /** 
       * @param {*} body.email
       *  @param {*} body.password
       */
    static async postLogin(req, res, next) {
        try {
            const { password, email } = req.body;
            const user = await userController.userLogin(email, password)
            const token = await userController.createUserToken(user);
            res.send({ ...user, token });
        } catch (err) {
            return next(err);
        }

    }
}
module.exports = UsersView;