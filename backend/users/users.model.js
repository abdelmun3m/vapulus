
const Collection = require('../utils/mogodb.wrapper');
const USER_COLLECTION = "Users"
class UserModel {

    /**
     * 
     * @param {string} email 
     * @param {string} name 
     * @param {string} password 
     */
    static async add(email, name, password) {
        let newUser = {
            email, name, password
        }
        const usersCollection = Collection.getCollection(USER_COLLECTION);
        const user = await usersCollection.insertOne(newUser);
        return user;
    }


    /**
     * 
     * @param {*} email 
     */
    static async getByEmail(email) {
        const usersCollection = Collection.getCollection(USER_COLLECTION);
        const user = await usersCollection.findOne({ email });
        return user
    }


}

module.exports = UserModel;