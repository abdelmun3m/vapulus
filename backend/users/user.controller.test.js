const userModel = require('./users.model');
jest.mock('./users.model');
const mailService = require('../utils/email.service');
jest.mock('../utils/email.service');
const { Logger } = require('../utils/log.service');
jest.mock('../utils/log.service');

const UserController = require('./users.controller');
describe('Add User', () => {
    describe('regiteation', () => {
        let messageSpy = ''
        beforeAll(() => {
            messageSpy = jest.spyOn(UserController, 'getPasswordCreationMessage').mockReturnValue("email text");
        });

        test('email is avilable', async () => {
            let email = 'test@gmail.com'
            let password = '1234'
            const emailResult = await UserController.userRegister(email, password);
            expect(userModel.getByEmail).toHaveBeenCalled();
            expect(messageSpy).toHaveBeenCalled();
            expect(emailResult).toEqual(email);
        });
        test('Email Used', async () => {
            let email = 'test@gmail.com'
            let password = '1234'
            const getEmailSpy = jest.spyOn(userModel, 'getByEmail').mockReturnValue(true);
            let error = '';
            try {
                const emailResult = await UserController.userRegister(email, password);
            } catch (err) {
                error = err
            }
            expect(getEmailSpy).toHaveBeenCalled();
            expect(error.message).toEqual('Sorrey! this email used by another user');
        });
    })
});
