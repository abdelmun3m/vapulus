
// class UserModel {

//     constructor() {

//         this.store = [];
//         this.add = jest.fn().mockImplementation((email, name, password) => {
//             let user = {
//                 email, name, password
//             }
//             this.store.push(user)
//             return user;
//         });

//         this.getByEmail = jest.fn().mockImplementation(email => {
//             const user = this.store.find(user => user.email === email);
//             return user;

//         })


//     }



// }

const store = [];
const UserModel = {

    add: jest.fn().mockImplementation((email, name, password) => {
        let user = {
            email, name, password
        }
        this.store.push(user)
        return user;
    }),
    getByEmail: jest.fn().mockImplementation(email => {
        const user = store.find(user => user.email === email);
        return user;
    })
}
module.exports = UserModel