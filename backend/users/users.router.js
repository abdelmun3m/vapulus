var express = require('express');
var router = express.Router();
const userView = require('./users.view');
const passport = require('../utils/passport')
/* GET users listing. */
router.post('/register', userView.postRegister);
router.post('/createUser', userView.postCreateUser);
router.post('/login', userView.postLogin);
router.post('/:token', userView.putUserPassword);
router.get('/:token', userView.verifyToken)



module.exports = router;
