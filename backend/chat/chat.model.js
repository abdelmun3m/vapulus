
const Collection = require('../utils/mogodb.wrapper');
const COLLECTION_NAME = "Chat"
class ChatModel {
    /**
     * 
     * @param {*} text 
     * @param {*} user 
     * @param {*} user 

     */
    static async addMessage(text, user, name) {
        let newMessage = {
            text, user, name,
            date: new Date()
        };
        const chatCollection =  Collection.getCollection(COLLECTION_NAME);
        const messageObject = await chatCollection.insertOne(newMessage);
        return messageObject;
    }
    static async getMessages() {
        const chatCollection = Collection.getCollection(COLLECTION_NAME);
        const messages = await chatCollection.findAll({});
        return messages
    }

}
module.exports = ChatModel