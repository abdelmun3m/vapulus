const socketController = require('./socker.controller');
const UserController = require('../users/users.controller')
const { Logger, LOG_STRATEGYS } = require('../utils/log.service')
const LoggerContext = new Logger(LOG_STRATEGYS.CONSOLE, "CHAT_SOCKET_CONTEXT");

module.exports = (io) => {
    io.use(async (socket, next) => {
        const token = socket.handshake.auth.token;
        const user = await UserController.verifyUserToken(token);
        if (!user) {
            LoggerContext.log(user, ` token faild ${token} `)
            socket.close()
        }
        next()
    })
    io.on('connection', async (socket) => {

        const email = socket.handshake.query.email;
        const name = socket.handshake.query.name;
        LoggerContext.log(email, `joind`)
        socketController.addConnection(email, name, socket.id)
        io.emit("usersList", socketController.getCurrentConnectionUsers());
        const latestMsssages = await socketController.getLatestMessages();
        socket.emit('latestMessages', latestMsssages)
        socket.on('disconnect', () => {
            LoggerContext.log(email, `disconnect`)
            socketController.removeConnection(socket.id);
            io.emit("usersList", socketController.getCurrentConnectionUsers());
        });
        socket.on('message', async (text) => {
            try {
                const newMessage = await socketController.addMessage(text, email, name);
                io.emit("new-message", newMessage);
            } catch (err) {
                socket.emit("newMessageFaild", err.message);
            }
        })
    })
}
