const { userRegister } = require("../users/users.controller");

const chatModel = require('../chat/chat.model');
const currectSockets = {
}
class SocketController {
    static addConnection(email, name, id) {
        currectSockets[id] = {
            email, name, id
        };
    }
    static removeConnection(id) {
        delete currectSockets[id];
    }
    static getCurrentConnectionUsers() {
        const users = Object.values(currectSockets).reduce((acc, user) => {
            if (!acc[user.email]) {
                acc[user.email] = user;
            }
            return acc;
        }, {});
        return Object.values(users)
    }

    static async addMessage(text, email, name) {
        const newMessage = await chatModel.addMessage(text, email, name);
        return newMessage;
    }
    static async getLatestMessages() {
        const latestMessages = await chatModel.getMessages();
        return latestMessages;

    }

}
module.exports = SocketController;