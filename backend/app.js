var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
require('dotenv').config()
var cors = require('cors')
var mongoDb = require('./utils/mogodb.wrapper');
const passport = require('./utils/passport')
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
swaggerDocument.servers[0].url = `http://localhost:${process.env.PORT}`;
mongoDb.connect();

var { USER_ROUTER } = require('./users');

var app = express();


app.use(cors())

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/api/users', USER_ROUTER);
app.use(passport.initialize());

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});


process.on('unhandledRejection', error => {
  // Will print "unhandledRejection err is not defined"
  throw error;
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  console.log(err)
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send(err.message);;
});

module.exports = app;
