class MailService {

    constructor() {
        this.send = jest.fn().mockReturnValue("sent")
    }
}
module.exports = MailService