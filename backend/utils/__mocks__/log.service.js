const LOG_STRATEGYS = {
    CONSOLE: 1,
}
class Logger {
    constructor(strategy, context) {
        this.strategy = new ConsoleStrategy();
        this.context = context;
    }
    async log(user, message) {

        try {
            this.strategy.log(this.context, user, message)
        } catch (err) {
            //
            console.log(err)

        }
    }

}

class ConsoleStrategy {
    log(context, user, message) {
        console.log(`test ---- ${user} in ${context} -- ${message}`)
    }
}

module.exports = { Logger, LOG_STRATEGYS };