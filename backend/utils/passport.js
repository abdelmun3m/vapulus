const passport = require('passport')
const BearerStrategy = require('passport-http-bearer').Strategy;
const UserController = require('../users/users.controller');

passport.use(new BearerStrategy(
    async function (token, done) {
        try {
            const user = await UserController.verifyUserToken(token)
            done(null, user);
        } catch (err) {
            return done(null, false, { message: 'invalid token !' });
        }
    }
));
module.exports = passport;