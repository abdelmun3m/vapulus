const { MongoClient } = require("mongodb");
const uri =
    `mongodb://${process.env.mongoServer}?retryWrites=true&writeConcern=majority&useUnifiedTopology=true`;

const client = new MongoClient(uri);
class Collection {
    constructor() {

    }
    async connect() {
        this.client = await client.connect();
        const database = this.client.db('Vapulus');
        const UsersCollection = database.collection("Users");
        UsersCollection.createIndexes('email', { unique: true });
        const ChatCollection = database.collection("Chat");
    }
    getCollection(name) {
        this.name = name
        return this
    }
    async findAll(query) {
        const database = this.client.db('Vapulus');
        const collection = database.collection(this.name);
        const result = await collection.find(query).toArray();
        return result || []
    }
    async findOne(query) {
        const database = this.client.db('Vapulus');
        const collection = database.collection(this.name);
        const result = await collection.findOne(query);
        return result

    }
    async insertOne(schema) {

        const database = this.client.db('Vapulus');
        const collection = database.collection(this.name);
        const result = await collection.insertOne(schema);
        return result.ops[0]

    }
    static async close() {
        await this.client.close();
    }

}
module.exports = new Collection();
