const LOG_STRATEGYS = {
    CONSOLE: 1,
    DB: 2,
    ELK: 3
}
class Logger {
    constructor(strategy, context) {
        this.strategy = new ConsoleStrategy();
        this.context = context
        if (strategy === LOG_STRATEGYS.DB) {
            this.strategy = new DBStrategy()
        } else if (strategy === LOG_STRATEGYS.ELK) {
            this.strategy = new ELKStrategy()
        } else if (strategy === LOG_STRATEGYS.CONSOLE) {
            this.strategy = new ConsoleStrategy();
        }
    }
    async log(user, message) {
        try {
            this.strategy.log(this.context, user, message)
        } catch (err) {
            //
        }
    }

}

class ConsoleStrategy {
    log(context, user, message) {
        console.log(`${user} in ${context} -- ${message}`)
    }
}

class DBStrategy {
    log(context, user, message) {
    }
}

class ELKStrategy {
    log(context, user, message) {
    }
}
module.exports = { Logger, LOG_STRATEGYS };