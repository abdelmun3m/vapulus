
const nodemailer = require('nodemailer');

class MailService {
    constructor(testEmail) {
        this.senderEmail = process.env.email;
        this.senderPassword = process.env.pass
        this.transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: this.senderEmail,
                pass: this.senderPassword
            }
        });
    }
    send(toMail, subject, html, afterSend) {
        let mailOptions = {
            from: process.env.email,
            to: toMail,
            subject: subject,
            html: html
        };
        this.transporter.sendMail(mailOptions, function (error, info) {
            afterSend(error, info);
        });
    }

}

module.exports = MailService;